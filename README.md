

# Installation

Using standard images
The simplest way is to download the appropriate SD card image from the NetBSD mirrors:

The Raspberry Pi 1 requires the ARMv6 rpi.img.gz.
The Raspberry Pi 2-3 can use the standard ARMv7 armv7.img.gz image.
The Raspberry Pi 3 can also use arm64.img.gz.
Decompress it and write it to the SD card:

$ gunzip armv7.img.gz
$ dd if=armv7.img of=/dev/rld0d conv=sync bs=1m progress=1
If you're not using NetBSD, your operating system's dd command's arguments may vary. On Windows, try Rawrite32.

The Raspberry Pi 4 requires the UEFI firmware. Write the UEFI firmware to the SD card, and then insert an USB drive with the standard NetBSD arm64.img written to it. The Pi will then boot from USB.

The Raspberry Pi 3 can also boot NetBSD from UEFI firmware, but the installation process is currently more complicated. However, there are some advantages, so you might want to try anyway.

## Unofficial



1) Create SD/MMC for RPI4 (efi, boot): 

````
  zcat  image... sdmmc > /dev/mmcblk0 
````


2.) Create the USB pendrive (root): 

````
  zcat  arm64.img.gz  > /dev/sdX...  
````


- Put the sd/mmc into the pi 4. 
- Put the usb pendrive into any usb port. 
- Power 


